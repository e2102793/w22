import React from "react";
import "./App.css";
import ComponentName from "./ComponentName";

function App() {
return (
<div className="App">
<ComponentName country = "Finland" />
<ComponentName country = "Sweden" />
</div>
);
}
export default App;